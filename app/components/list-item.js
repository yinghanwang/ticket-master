(function() {
	'use strict';

	var ListItem = function() {
		this.baseCls = 'insenble-list-item';
		this.ele = null;
		this.image = null;
		this.price = null;
		this.title = null;
		this.name = null;
		this.type = null;
		this.template = [
			'<div class="insenble-flex-left">',
				'<div class="li-item__image"></div>',
				'<div class="li-item__title">',
					'<div class="li-item-title__name">name</div>',
					'<div class="li-item-title__type">type</div>',
				'</div>',
			'</div>',
			'<div class="li-item__arrow"></div>',
		].join('');
		this.clickItemEvent = [];
		this.data = null;
	};

	ListItem.prototype.init = function() {
		this.initEle();
		this.initEvent();
	};

	ListItem.prototype.initEle = function() {
		this.ele = document.createElement('div');
		this.ele.className = this.baseCls;
		this.ele.innerHTML = this.template;
		this.image = this.ele.querySelector('.li-item__image');
		this.price = this.ele.querySelector('.li-item__price');
		this.title = this.ele.querySelector('.li-item__title');
		this.name = this.ele.querySelector('.li-item-title__name');
		this.type = this.ele.querySelector('.li-item-title__type');
	};

	ListItem.prototype.registerEvt = function(handler) {
		this.clickItemEvent.push(handler);
	};

	ListItem.prototype.initEvent = function() {
		var me = this;
		this.ele.addEventListener('click', me.onClickItem.bind(me));
	};

	ListItem.prototype.onClickItem = function() {
		var cie;
		for(var i = 0 ; i < this.clickItemEvent.length; i++) {
			cie = this.clickItemEvent[i];
			cie.fn.apply(cie.scope, [this.data]);
		}
	};

	ListItem.prototype.setData = function(data) {
		this.data = data;
		this.updateData(data);
	};

	ListItem.prototype.updateData = function(newData) {
		this.refresh(newData);
	};

	ListItem.prototype.refresh = function(data) {
		data = data || this.data;
		var d = data._embedded.attractions;
		this.image.style['background-image'] = 'url(' + d[0].images[0].url + ')';
		this.name.innerText = d[0].name;
		this.type.innerText = 'artist';
	};

	module.exports = ListItem;
	
})();