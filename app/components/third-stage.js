(function() {
	'use strict';

	var $ = require('jquery');

	var ThirdStage = function(config) {
		config = config || {};
		this.baseCls = 'insenble-third-stage';
		this.mainImage = null;
		this.parent = document.querySelector('.' + config.parent);
		this.backBtn = null;
		this.listEle = null;
		this.template = [
			'<div>',
				'<div class="item-detail-header">',
					'<div class="item-detail-back"></div>',
					'<div class="item-detail-main-image"></div>',
				'</div>',
				'<div class="item-detail-list-header">',
					'<div>LOCATION</div>',
					'<div>DATE</div>',
					'<div>CONCERT</div>',
					'<div>EXPERIENCE</div>',
				'</div>',
				'<div class="item-detail-list-wrap">',
					'<div class="third-stage-item">',
						'<div>sky scanner</div>',
						'<div class="third-item-wrap">',
							'<div class="third-price">€170</div>',
							'<div class="third-price">€170</div>',
							'<div class="third-price">€170</div>',
						'</div>',
					'</div>',
					'<div class="third-stage-item">',
						'<div>airbnb</div>',
						'<div class="third-item-wrap">',
							'<div class="third-price">€170</div>',
							'<div class="third-price">€170</div>',
							'<div class="third-price">€170</div>',
						'</div>',
					'</div>',
					'<div class="third-stage-item">',
						'<div>just eat</div>',
						'<div class="third-item-wrap">',
							'<div class="third-price">€170</div>',
							'<div class="third-price">€170</div>',
							'<div class="third-price">€170</div>',
						'</div>',
					'</div>',
				'</div>',
			'</div>'
		].join('');
		this.ele = null;
		this.data = null;

		this.events = [];
	};

	ThirdStage.prototype.init = function() {
		this.initEle();
		this.initEvent();
	};

	ThirdStage.prototype.initEle = function() {
		this.ele = document.createElement('div');
		this.ele.className = this.baseCls;
		this.ele.innerHTML = this.template;
		this.backBtn = this.ele.querySelector('.item-detail-back');
		this.mainImage = this.ele.querySelector('.item-detail-main-image');
		this.listEle = this.ele.querySelector('.item-detail-list-wrap');
		this.parent.appendChild(this.ele);
	};

	ThirdStage.prototype.initEvent = function() {
		var me = this;
		this.backBtn.addEventListener('click', me.onClickBackBtn.bind(me));
	};

	ThirdStage.prototype.onClickBackBtn = function() {
		this.triggerEvent();
	};

	ThirdStage.prototype.triggerEvent = function() {
		var e;
		for(var i = 0; i < this.events.length; i++) {
			e = this.events[i];
			e.fn.apply(e.scope, []);
		}
	};

	ThirdStage.prototype.registerEvent = function(handler) {
		this.events.push(handler);
	};
	ThirdStage.prototype.hide = function(speed) {
		var me = this;
		$(me.ele).fadeOut(speed);
	};

	ThirdStage.prototype.show = function(speed) {
		var me = this;
		$(me.ele).fadeIn(speed);
	};

	ThirdStage.prototype.setData = function(data) {
		this.data = data;
		this.updateData(data);
	};

	ThirdStage.prototype.updateData = function(newData) {
		this.refresh(newData);
	};

	ThirdStage.prototype.refresh = function(data) {
		this.mainImage.style['background-image'] = 'url(' + data.images[0].url + ')';
		var listData = data.detailList;
		var d;
		var html = '';
		// for(var i = 0; i < listData.length;i++) {
		// 	d = listData[i];
		// 	html += this.makeItem(d);
		// }
		// this.listEle.innerHTML = html;
	};

	ThirdStage.prototype.makeItem = function(data) {
		var temp = [
			'<div class="item-detail-list">',
			'<div class="insenble-average insenble-item-detail-location">'+ data.location +'</div>',
			'<div class="insenble-average insenble-item-detail-date">'+ data.date +'</div>',
			'<div class="insenble-average insenble-item-detail-concert">'+ data.concert +'</div>',
			'<div class="insenble-average insenble-item-detail-experience">'+ data.experience +'</div>',
			'</div>'
		].join('');
		return temp;
	};

	module.exports = ThirdStage;
	
})();