(function() {
	'use strict';

	var $ = require('jquery');

	var ItemDetail = function(config) {
		config = config || {};
		this.baseCls = 'insenble-item-detail';
		this.mainImage = null;
		this.parent = document.querySelector('.' + config.parent);
		this.backBtn = null;
		this.listEle = null;
		this.template = [
			'<div>',
				'<div class="item-detail-header">',
					'<div class="item-detail-back"></div>',
					'<div class="item-detail-main-image"></div>',
				'</div>',
				'<div class="item-detail-list-header">',
					'<div>LOCATION</div>',
					'<div>DATE</div>',
					'<div>CONCERT</div>',
					'<div>EXPERIENCE</div>',
				'</div>',
				'<div class="item-detail-list-wrap"></div>',
			'</div>'
		].join('');
		this.ele = null;
		this.data = null;

		this.events = {};
	};

	ItemDetail.prototype.init = function() {
		this.initEle();
		this.initEvent();
	};

	ItemDetail.prototype.initEle = function() {
		this.ele = document.createElement('div');
		this.ele.className = this.baseCls;
		this.ele.innerHTML = this.template;
		this.backBtn = this.ele.querySelector('.item-detail-back');
		this.mainImage = this.ele.querySelector('.item-detail-main-image');
		this.listEle = this.ele.querySelector('.item-detail-list-wrap');
		this.parent.appendChild(this.ele);
	};

	ItemDetail.prototype.initEvent = function() {
		var me = this;
		this.backBtn.addEventListener('click', me.onClickBackBtn.bind(me));
	};

	ItemDetail.prototype.onClickBackBtn = function() {
		this.triggerEvent('clickbackbtn');
	};

	ItemDetail.prototype.triggerEvent = function(topic) {
		var e;
		for(var i = 0; i < this.events[topic].length; i++) {
			e = this.events[topic][i];
			e.fn.apply(e.scope, []);
		}
	};

	ItemDetail.prototype.registerEvent = function(topic, handler) {
		this.events[topic] = this.events[topic] || [];
		this.events[topic].push(handler);
	};
	ItemDetail.prototype.hide = function(speed) {
		var me = this;
		$(me.ele).fadeOut(speed);
	};

	ItemDetail.prototype.show = function(speed) {
		var me = this;
		$(me.ele).fadeIn(speed);
	};

	ItemDetail.prototype.setData = function(data) {
		this.data = data;
		this.updateData(data);
	};

	ItemDetail.prototype.updateData = function(newData) {
		this.refresh(newData);
	};

	ItemDetail.prototype.refresh = function(data) {
		var me = this;
		this.mainImage.style['background-image'] = 'url(' + data.images[0].url + ')';
		var listData = data.detailList;
		var d;
		var html = '';
		for(var i = 0; i < listData.length;i++) {
			d = listData[i];
			html += this.makeItem(d);
		}
		this.listEle.innerHTML = html;
		this.listEle.onclick = function() {
			me.triggerEvent('clickitem');
		};
	};

	ItemDetail.prototype.makeItem = function(data) {
		var temp = [
			'<div class="item-detail-list">',
			'<div class="insenble-average insenble-item-detail-location">'+ data.location +'</div>',
			'<div class="insenble-average insenble-item-detail-date">'+ data.date +'</div>',
			'<div class="insenble-average insenble-item-detail-concert">'+ data.concert +'</div>',
			'<div class="insenble-average insenble-item-detail-experience">'+ data.experience +'</div>',
			'</div>'
		].join('');
		return temp;
	};

	module.exports = ItemDetail;
	
})();