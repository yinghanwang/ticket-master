(function() {
    'use strict';
    var $ = require('jquery');
    var __ = require('../app/util/common.js');
    var ListItem = require('../app/components/list-item.js');
    var ItemDetail = require('../app/components/item-detail.js');
    var ThirdStage = require('../app/components/third-stage.js');

    var TM_ROOT = 'https://app.ticketmaster.com/discovery/v2/';
    var APIKEY = 'apikey=AHEexHyOg4nPvXLsVlEAqACgEbnwkXHC';
    var SPOTIFY_URL = 'https://api.spotify.com/v1/artists/1vCWHaC5f2uS3yhpwWbIA6/albums?album_type=SINGLE';
    var attractiontype = 'attractions.json?keyword={0}&';
    var eventType = 'events.json?';

    var listData = [];
    var listView = [];
    var body = document.body;
    var listEle = document.querySelector('.list');
    var itemDetail = createItemDetailView();
    var thirdStage = createThirdStage();

    var mockLocatioin = [
    	{
    		location: 'dublin',
    		date: '01-02-2018',
    		concert: 'sold out',
    		experience: 'sold out'
    	},
    	{
    		location: 'berlin',
    		date: '01-02-2018',
    		concert: 'sold out',
    		experience: 'sold out'
    	},
    	{
    		location: 'london',
    		date: '01-02-2018',
    		concert: 'sold out',
    		experience: 'sold out'
    	},
    	{
    		location: 'dublin',
    		date: '01-02-2018',
    		concert: 'sold out',
    		experience: 'sold out'
    	},
    	{
    		location: 'cork',
    		date: '01-02-2018',
    		concert: 'sold out',
    		experience: 'sold out'
    	},
    	{
    		location: 'berlin',
    		date: '01-02-2018',
    		concert: 'sold out',
    		experience: 'sold out'
    	},
    	{
    		location: 'london',
    		date: '01-02-2018',
    		concert: 'sold out',
    		experience: 'sold out'
    	},
    	{
    		location: 'dublin',
    		date: '01-02-2018',
    		concert: 'sold out',
    		experience: 'sold out'
    	},
    	{
    		location: 'cork',
    		date: '01-02-2018',
    		concert: 'sold out',
    		experience: 'sold out'
    	},
    	{
    		location: 'berlin',
    		date: '01-02-2018',
    		concert: 'sold out',
    		experience: 'sold out'
    	},
    	{
    		location: 'london',
    		date: '01-02-2018',
    		concert: 'sold out',
    		experience: 'sold out'
    	},
    	{
    		location: 'dublin',
    		date: '01-02-2018',
    		concert: 'sold out',
    		experience: 'sold out'
    	},
    	{
    		location: 'cork',
    		date: '01-02-2018',
    		concert: 'sold out',
    		experience: 'sold out'
    	}
    ];

    var apiHandler = {
        fetchData: function(config) {
        	var me = this;
            $.ajax({
                url: config.url,
                type: 'GET',
                error: config.failCallback,
            }).done(config.successCallback);
        }
    };

    function createThirdStage() {
    	var t = new ThirdStage({
	    	parent: 'third-stage-page'
	    });
	    t.init();
	    t.hide(0);
	    t.registerEvent({
	    	scope: null,
	    	fn: onClickBackInThirdStage
	    });
	    body.appendChild(t.ele);
	    return t;
    }

    function onClickBackInThirdStage() {
    	itemDetail.show(200);
    	thirdStage.hide(0);
    }

    function createItemDetailView() {
    	var t = new ItemDetail({
	    	parent: 'item-detail-page'
	    });
	    t.init();
	    t.hide(0);
	    t.registerEvent('clickbackbtn', {
	    	scope: null,
	    	fn: onClickBackInItemDetail
	    });
	    t.registerEvent('clickitem', {
	    	scope: null,
	    	fn: onClickBackInItemDetailItem
	    });
	    body.appendChild(t.ele);
	    return t;
    }

    function onClickBackInItemDetailItem() {
    	itemDetail.hide(200);
    	thirdStage.show(200);
    }

    function onClickBackInItemDetail() {
    	$(listEle).show();
    	itemDetail.hide(200);
    }

    /**
     * this is where everything starts
     * @param {}
     */
    function enterpriseEngage() {
    	getSpotifyData();
    }

    /**
     * ;adsjk;jads;kfj;adsf
     * @param {}
     */
    function getSpotifyData() {
    	apiHandler.fetchData({
	    	url: SPOTIFY_URL,
	    	successCallback: getArtistsFromSpotify
	    });
    }

    /**
     * get artist from spotify
     * @param {}
     */
    function getArtistsFromSpotify(data) {
    	var items = data.items, artists = [], a;

    	for(var i =0, ln = items.length; i < ln; i++){
    		a = items[i].artists;
    		for(var j = 0 ; j < a.length ;j++) if(artists.indexOf(a[j].name) === -1) artists.push(a[j].name);
    	}
    	onSpotifyArtistsReady(artists);
    	return artists;
    }

    /**
     * action taken when artist is ready
     * @param {String} artists
     */
    function onSpotifyArtistsReady(artists) {
    	apiHandler.fetchData({
	        url: TM_ROOT + (eventType.replace('{0}', name)) + APIKEY,
	        successCallback: function(data) {
	        	console.log(data);
	            createList(data._embedded.events);
	        }
	    });
    }

    /**
     * get event from ticket master by artist name
     * @param {}
     */
    function getEventsFromTmByArtistsName(artists) {
    	for(var i = 0 ; i < artists.length; i++) {
    		getSingleEventsByName(artists[i], artists.length);
    	}
    }

    function onEventsFromTm(data) {
    	console.log(data);
    	listView = createList(data._embedded.events);
    	console.log(listView);
    }


    function getSingleEventsByName(name, expectedNum) {
    	apiHandler.fetchData({
	        url: TM_ROOT + (eventType.replace('{0}', name)) + APIKEY,
	        successCallback: function(data) {
	        	if(data) {
	        		createList(data);
	        	}
	        }
	    });
    }

    

    function createList(data) {
	    var list = [];
    	var li;
    	var frag = document.createDocumentFragment();
    	for(var i = 0; i < data.length; i++) {
	    	li = new ListItem();
	    	li.init();
	    	li.setData(data[i]);
	    	li.registerEvt({
	    		scope: null,
	    		fn: detectEvt
	    	});
	    	list.push(li);
	    	frag.appendChild(li.ele);
    	}
    	listEle.appendChild(frag);
    	console.log(list);
    	return list;
    }

    function detectEvt(data) {
    	$(listEle).hide();
    	itemDetail.show(200);
    	data.detailList = mockLocatioin;
    	itemDetail.setData(data);
    	thirdStage.setData(data);
    }

    var appEle = document.querySelector('.insenble');

    enterpriseEngage();

})();
